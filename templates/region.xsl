<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
	<xsl:output method="html"/>
    <xsl:template match="/">
    	<div>
        	<xsl:attribute name="class">            	
                <xsl:for-each select="//classes_array/item">
                	<xsl:value-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
            </xsl:attribute>
            <xsl:value-of select="//content" disable-output-escaping="yes"/>
        </div>
    </xsl:template>
</xsl:stylesheet>