<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    <xsl:output method="html"/>
    <xsl:template match="/root">
    	<xsl:variable name='nid' select="./nid"/>
        <xsl:if test="//page != 1">
            <h2 class="title">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select='php:function("url", concat(string("node/"), ./nid))'/>
                    </xsl:attribute>
                    <xsl:value-of select="./title"/>
                </a>
            </h2>
        </xsl:if>
        <xsl:if test="//unpublished != 1">
        	<div class="unpublished">
            	<xsl:value-of select="php:function('t', 'Unpublished')"/>
            </div>
        </xsl:if>
        <xsl:if test="//display_submitted &gt; 0">
            <div class='submitted'>
                Submitted by <xsl:value-of select="./user/name"/> on <xsl:value-of select="./created"/>
            </div>
        </xsl:if>
        <div class='content'>
        	<xsl:value-of disable-output-escaping="yes" select="content"/>
        </div>
        <xsl:if test="string-length(//terms) &gt; 0">
        	<div class='terms'>
        		<xsl:value-of select="//terms" disable-output-escaping="yes"/>
        	</div>
        </xsl:if>
        <xsl:if test='count(./node/taxonomy/*) &gt; 0 and false'>
        	<xsl:call-template name='taxonomy'>
            	<xsl:with-param name='terms' select="./node/taxonomy/*"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <xsl:template name='taxonomy'>
    	<xsl:param name='terms'/>
        <ul>
            <xsl:for-each select="$terms">
            	<xsl:sort data-type="number" select="./name"/>
                <li>
                    <a>
                        <xsl:attribute name='href'>
                            <xsl:value-of select='concat(string("taxonomy/term/"), ./tid)'/>
                        </xsl:attribute>
                        <xsl:value-of select='./name'/>            
                    </a>
                </li>     
            </xsl:for-each>
    	</ul>
    </xsl:template>
</xsl:stylesheet>