<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl" exclude-result-prefixes="php">
	<xsl:output method="html"/>
	<xsl:template match='/root'>
		<xsl:variable name='block' select='./block'/>
		<xsl:if test="string-length($block/title | $block/subject) &gt; 0">
			<h4><xsl:value-of select="$block/title | $block/subject"/></h4>
		</xsl:if>
		<div>
			<xsl:attribute name="class">
				<xsl:value-of select="string('block ')"/>
				<xsl:for-each select="//classes_array/item">
                	<xsl:value-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
			</xsl:attribute>
			<xsl:value-of disable-output-escaping="yes" select="$block/content"/>
		</div>
	</xsl:template>
</xsl:stylesheet>