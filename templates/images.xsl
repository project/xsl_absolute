<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
	<xsl:template name='images'>
		<xsl:param name='node'/>
        <ul>	
            <xsl:for-each select='$node/field_image/item'>
                <xsl:variable name='image' select='.'/>
                <xsl:variable name='url' select='concat(string("node/"), $node/nid)'/>
                <xsl:variable name='fileName' select='$image/filename'/>
                <xsl:variable name='srcBaseAlpha' select='concat(string("sites/default/files/imagecache/"), $preset)'/>
                <xsl:variable name='srcBaseBravo' select='php:function("url", $srcBaseAlpha)'/>
               
                <li>                
                    <a>
                        <xsl:attribute name='href'>
                            <xsl:value-of select='php:function("url", $url)'/>
                        </xsl:attribute>
                        <img>
                            <xsl:attribute name='src'>
                                <xsl:value-of select='concat($srcBaseBravo, string("/"), $fileName)'/>
                            </xsl:attribute>
                            <xsl:attribute name='title'>
                                <xsl:value-of select='$image//title'/>
                            </xsl:attribute>  
                            <xsl:attribute name='alt'>
                                <xsl:value-of select='$image//alt'/>
                            </xsl:attribute>                    
                        </img>
                    </a>				
                </li>			
            </xsl:for-each>
        </ul>	
	</xsl:template>
</xsl:stylesheet>