<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
 	<xsl:template name="node">
    	<xsl:param name='node'/>
    	<xsl:variable name='nid' select="$node/nid"/>
        <a>
        	<xsl:attribute name="href">
            	<xsl:value-of select='php:function("url", concat(string("node/"), $node/nid))'/>
            </xsl:attribute>
        	<h2><xsl:value-of select="$node/title"/></h2>
        </a>
        <div class='content'>
        	<xsl:value-of disable-output-escaping="yes" select="$node/body"/>
        </div>
        <xsl:if test='count($node/taxonomy/*) &gt; 0'>        	
        	<xsl:call-template name='taxonomy'>
            	<xsl:with-param name='node' select="$node"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>