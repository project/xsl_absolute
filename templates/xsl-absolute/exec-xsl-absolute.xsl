<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    <xsl:template match="/root">    
        <xsl:call-template name="main">
        	<xsl:with-param name='arg' select='.'/>
        </xsl:call-template>       
    </xsl:template>
    <xsl:template name="main">
    	<xsl:param name="arg"/>
    </xsl:template>
</xsl:stylesheet>