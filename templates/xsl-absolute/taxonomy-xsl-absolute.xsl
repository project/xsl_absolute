<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
	<xsl:template name='taxonomy'>
		<xsl:param name='node'/>
        <ul>
            <xsl:for-each select='$node/taxonomy/item'>
                <xsl:variable name='term' select='.'/>
                <xsl:variable name='tid' select='$term/tid'/>
                <xsl:variable name='name' select='$term/name'/>
                <xsl:variable name='url' select='concat(string("taxonomy/term/"), $tid)'/>
                <li>
                    <a>
                        <xsl:attribute name='href'>
                            <xsl:value-of select='php:function("url", $url)'/>
                        </xsl:attribute>
                        <xsl:value-of select='$name'/>					
                    </a>
                </li>			
            </xsl:for-each>
        </ul>
	</xsl:template>
</xsl:stylesheet>