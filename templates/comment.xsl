<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    <xsl:output method="html"/>
    <xsl:template match="/root">
    	<div>
    		<xsl:attribute name="class">
                <xsl:for-each select="//classes_array/item">
                	<xsl:value-of select="."/>
                	<xsl:text> </xsl:text>
                </xsl:for-each>               
    		</xsl:attribute>
            <h3 class='title'>
            	<xsl:value-of select='./comment/subject'/>
                <xsl:if test="./comment/new &gt; 0">
                	<span class='new'><xsl:value-of select="./new"/></span>
                </xsl:if>
            </h3>
            <xsl:if test="//unpublished = 1">
            	<div class='unpublished'>
                <xsl:value-of select="php:function('t', 'Unpublished')"/>
                </div>
            </xsl:if>
            <div class='submitted'>
            	Submitted by <xsl:value-of select="./user/name"/> on <xsl:value-of select="./created"/>
            </div>
    		<div class='content'>
            	<xsl:value-of select="./comment/comment" disable-output-escaping="yes" />
            </div>
    	</div>
    </xsl:template>
</xsl:stylesheet>