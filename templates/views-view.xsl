<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <div>
          <xsl:attribute name='class'>
            <xsl:for-each select="//classes_array/item">
                <xsl:value-of select="."/>
                <xsl:text> </xsl:text>
            </xsl:for-each> 
          </xsl:attribute>
          
          <xsl:if test="string-length(//admin_links) &gt; 0">
           <div class="views-admin-links views-hide">
              <xsl:value-of select="//admin_links" disable-output-escaping="yes"/>
           </div>
          </xsl:if>
          
          <xsl:if test="string-length(//header) &gt; 0">
            <div class='view-header'>
                <xsl:value-of select="//header" disable-output-escaping="yes"/>
            </div>
          </xsl:if>
          
          <xsl:if test="string-length(//exposed) &gt; 0">
            <div class='view-filters'>
                <xsl:value-of select="//exposed" disable-output-escaping="yes"/>
            </div>
          </xsl:if>
          <xsl:if test="string-length(//attachment_before) &gt; 0">
           <div class="attachment attachment-before">
            <xsl:value-of select="//attachment_before" disable-output-escaping="yes"/>
           </div>
          </xsl:if>
          <xsl:if test="string-length(//rows) &gt; 0">
            <div class="view-content">
              <xsl:value-of select="//rows" disable-output-escaping="yes"/>
            </div>
          </xsl:if>
          <xsl:if test="string-length(//empty) &gt; 0">
            <div class="view-empty">
              <xsl:value-of select="//empty" disable-output-escaping="yes"/>
            </div>
          </xsl:if>
          <xsl:if test="string-length(//pager/use_pager) &gt; 0">
          	<div class='view-pager'>
            	<xsl:value-of select="//pager/element" disable-output-escaping="yes"/>
          	</div>
          </xsl:if> 
          
          <xsl:if test="string-length(//attachment_after) &gt; 0">
            <div class="attachment attachment-after">
              <xsl:value-of select="//attachment_after" disable-output-escaping="yes"/>
            </div>
          </xsl:if>
          
          <xsl:if test="string-length(//more) &gt; 0">
            <xsl:value-of select="//more" disable-output-escaping="yes"/>
          </xsl:if>
          
          <xsl:if test="string-length(//footer) &gt; 0">
            <div class='view-footer'>
                <xsl:value-of select="//footer" disable-output-escaping="yes"/>
            </div>
          </xsl:if>
          
          <xsl:if test="string-length(//feed_icon) &gt; 0">
            <div class='feed-icon'>
              <xsl:value-of select="//feed_icon" disable-output-escaping="yes"/>
            </div>   
          </xsl:if>
        </div> <!-- /.view -->
	</xsl:template>
</xsl:stylesheet>