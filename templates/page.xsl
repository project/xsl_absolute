<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
	<xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>   
    <xsl:template name='page' match="/root">
        <xsl:param name='vars' select='.'/>
        <html>
            <head>
                <title><xsl:value-of select='$vars/head_title'/></title>
                <xsl:value-of disable-output-escaping="yes" select='$vars/head'/>
                <xsl:value-of disable-output-escaping="yes" select='$vars/styles'/>
                <xsl:value-of disable-output-escaping="yes" select='$vars/scripts'/>
            </head>
            <body>
            	<xsl:attribute name='class'>
                	<xsl:for-each select="//classes_array/item">
                        <xsl:value-of select="."/>
                        <xsl:text> </xsl:text>
                	</xsl:for-each> 
                </xsl:attribute>
                <div id="page-wrapper">
                	<div id="page">
    					<div id="header">
                        	<div class="section clearfix">
                				<div id="logo-floater">
                                    <h2>
                                        <a>
                                            <xsl:attribute name='href'>
                                                <xsl:value-of select='$vars/front_page'/>
                                            </xsl:attribute>
                                            <xsl:attribute name='title'>
                                                <xsl:value-of select='$vars/site_title'/>
                                            </xsl:attribute>
                                            <img>
                                                <xsl:attribute name='src'>
                                                    <xsl:value-of select="$vars/logo"/>
                                                </xsl:attribute>
                                                <xsl:attribute name='title'>
                                                    <xsl:value-of select='$vars/site_title'/>
                                                </xsl:attribute>
                                            </img>
                                            <xsl:for-each select='$vars/site_name | $vars/site_title | $vars/site_slogan'>
                                                <xsl:value-of select='.'/>                            
                                            </xsl:for-each>                                   
                                        </a>
                                    </h2>
                                </div>
                                <div id="header-region" class="clear-block">
                                	<xsl:value-of disable-output-escaping="yes" select='$vars/header'/>
                                </div>
                           </div>
                        </div> <!-- #header -->
                        <div id="main-wrapper">
                        	<div id="main">
								<xsl:attribute name="class">
                                	<xsl:text>clearfix </xsl:text>
                                </xsl:attribute>
                                
      							<div id="content" class="column">
                                	<div class="section">
                                    	
                                        <xsl:if test="string-length(//mission) &gt; 0">
                                        	<div id="mission">
                                            	<xsl:value-of select="//mission" disable-output-escaping="yes"/>
                                            </div>
                                        </xsl:if>
                                        
                                        <div id="highlight">
                                        	<xsl:value-of select='//highlight' disable-output-escaping="yes"/>
                                        </div>
                                        
                                        <xsl:value-of select="//breadcrumb" disable-output-escaping="yes"/>
                                        
                                        <xsl:if test="string-length(//title) &gt; 0">
                                        	<h1 class="title">
                                            	<xsl:value-of select="//title"/>
                                            </h1>
                                        </xsl:if>
                                        
                                        <div id="messages">
                                            <xsl:value-of disable-output-escaping="yes" select='$vars/messages'/>
                                        </div>
                                        
                                        <xsl:if test="string-length(//tabs) &gt; 0">
                                        	<xsl:value-of select="//tabs" disable-output-escaping="yes"/>
                                        </xsl:if>
                                        
                                        <div id='content-area'>
                                            <xsl:value-of disable-output-escaping="yes" select='$vars/content'/>
                                        </div>
                                   </div>
                               </div><!-- #content -->        
                                <xsl:if test="string-length(//primary_links_menu) &gt; 0">
                                     <div id="navigation">
                                        <div class="section clearfix">
                                            <xsl:value-of select="//primary_links_menu" disable-output-escaping="yes"/>
                                        </div>
                                     </div>
                                </xsl:if>
                                
                                <div id='sidebar-first' class='region-sidebar-first'>
                                    <xsl:value-of disable-output-escaping="yes" select='$vars/sidebar_first'/>
                                </div>
                                <div id='sidebar-second' class='region-sidebar-second'>
                                    <xsl:value-of disable-output-escaping="yes" select='$vars/sidebar_second'/>
                                </div>
                                                                                 
                            </div>
                        </div><!-- #main-wrapper -->
                        <xsl:if test="string-length(//footer) &gt; 0">
                            <div id="footer">
                                <div class="section">
                                	 <xsl:if test="string-length(//secondary_links_menu) &gt; 0">
                                         <xsl:value-of select="//secondary_links_menu" disable-output-escaping="yes"/>
                                     </xsl:if>
                                    <xsl:if test="string-length(//footer_message) &gt; 0">
                                        <div id='footer-message'>
                                            <xsl:value-of disable-output-escaping="yes" select="//footer_message"/>
                                        </div>
                                    </xsl:if>
                                    <xsl:value-of select="//footer" disable-output-escaping="yes"/>
                                </div>
                            </div> <!-- #footer -->
                        </xsl:if>                   
                    </div>
            	</div>
                <xsl:value-of select="//page_closure" disable-output-escaping="yes"/>
                <xsl:value-of select="//closure" disable-output-escaping="yes"/>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>