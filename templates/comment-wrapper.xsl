<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:php="http://php.net/xsl">
    <xsl:output method="html"/>
 	<xsl:template match="/">
    	<div>
        	<xsl:attribute name="class">
            	<xsl:text>comment-wrapper </xsl:text>
                <xsl:for-each select="//classes_array/item">
                	<xsl:value-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
            </xsl:attribute>
            <xsl:if test="//node/type != 'forum'">
                <h3 class="title">
                    <xsl:value-of select="php:function('t', 'Comments')"/>
                </h3>
            </xsl:if>
            <xsl:value-of select='//content' disable-output-escaping="yes"/>
        </div>
    </xsl:template> 	
</xsl:stylesheet>