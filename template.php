<?php

function xsl_absolute_theme(){
  return array(
    'xsl_theme' => array(
      'function' => 'xsl_absolute_template',
      'arguments' => array(
        'template_file' => false,
        'node' => false,
        'options' => array()      
      )
    ),    
    'xsl_theme_taxonomy' => array(
      'function' => 'xsl_absolute_render_taxonomy',
      'arguments' => array(
        'node' => false,
        'options' => array()      
      )    
    ),
    'xsl_theme_images' => array(
      'function' => 'xsl_absolute_render_images',
      'arguments' => array(
        'node' => false,
        'preset' => false,
        'options' => array()      
      )    
    ),
    'xsl_theme_node' => array(
      'function' => 'xsl_absolute_render_node',
      'arguments' => array(
        'node' => false,
        'options' => array()   
      )
    ),
    'region' => array(
          'arguments' => array('elements' => NULL),
          //'pattern' => 'region_',
            'path' => drupal_get_path('theme', 'xsl_absolute') . '/',
            'template' => 'region',
          // We manually register the preprocess and process functions, since
          // Drupal won't auto-register template_preprocess or process functions.
          'preprocess functions' => array(
            'template_preprocess',
            'xsl_absolute_preprocess',
            'xsl_absolute_preprocess_region'
           ),
       ),
  );
}

/**
 * Return a set of blocks available for the current user.
 *
 * @param $region
 *   Which set of blocks to retrieve.
 * @param $show_blocks
 *   A boolean to determine whether to render sidebar regions or not. Should be
 *   the same value as passed to theme_page.
 * @return
 *   A string containing the themed blocks for this region.
 * @note 
 *   Groups the regions together such that you can use the region template.
 *
 */
function xsl_absolute_blocks($region, $show_blocks = NULL) {
  // Since Drupal 6 doesn't pass $show_blocks to theme_blocks, we manually call
  // theme('blocks', NULL, $show_blocks) so that this function can remember the
  // value on later calls.
  
  static $render_sidebars = TRUE;
  if (!is_null($show_blocks)) {
    $render_sidebars = $show_blocks;
  }
  
  $render_sidebars = true;
 
  
  // If zen_blocks was called with a NULL region, its likely we were just
  // setting the $render_sidebars static variable.
  if ($region) {
    $output = '';

    // If $renders_sidebars is FALSE, don't render any region whose name begins
    // with "sidebar_".
    if (($render_sidebars || (strpos($region, 'sidebar_') !== 0)) && ($list = block_list($region))) {
      foreach ($list as $key => $block) {
        // $key == module_delta
        $output .= theme('block', $block);
      }
    }

    // Add any content assigned to this region through drupal_set_content() calls.
    $output .= drupal_get_content($region);

    $elements['#children'] = $output;
    $elements['#region'] = $region;
    return $output ? theme('region', $elements) : '';
  }
}

/**
 * Implementation of hook_breadcrumb
 * Note: If you override this, be sure that your separator is XML Safe
 */
function xsl_absolute_breadcrumb($breadcrumb){
  
  return '<div class="breadcrumb">' . implode(" &gt; ", $breadcrumb) . "</div>";   
  
}

/**
 *  
 * Receives a data transfer object of any kind, could be a user object or a node.  Could be the variables from a theme 
 * method or preprocess function.
 * @param $arg 
 *  data object that is to be serialized into XML and evaluated against the stylesheet.
 * @param $opts
 *    options structure. 
 *    Properties:
 *      decorator_class -- the class used to serialize the data object
 *      decorator_instance -- you can provide a decorator instance if you've already constructed your own
 *      template_name -- The name of the primary xsl:template that you wish to invoke.
 *      template_param_name -- The name of the variable that the invoking template is expecting, such as node or taxonomy.
 *      template_selector -- The selector given to the xsl:with-param element, defines the parameter given to the invoking template.
 *      template_files -- An array of either paths to xsl files or DOMDocument instance's of an XSL, which will have all of it's templates imported into the primary stylesheet.
 *      php_functions -- An array of strings referencing methods that will be available to the stylesheet.
 * 
 * @return
 *   The HTML result of evaluating the data XML against the stylesheet
 */

function xsl_absolute_template($arg, $opts = array()){

  $opts = array_merge(array(
    'decorator_class' => 'node_dto_decorator',
    'decorator_instance' => false,
    'template_name' => 'main',
    'template_param_name' => 'arg',
    'template_selector' => '.',
    'template_files' => array()    
  ), $opts);
  $sheet = xsl_absolute_build_sheet('exec-xsl-absolute.xsl');
  //using params didn't work, it wouldn't evaluate the variable, just considered it a string literal.
  $call = $sheet -> getElementsByTagName('call-template')->item(0);
  $call->setAttribute("name", $opts['template_name']);
  $param = $sheet -> getElementsByTagName('with-param')->item(0);
  $param->setAttribute("select", $opts['template_selector']);
  $param->setAttribute("name", $opts['template_param_name']);
   
  xsl_absolute_import_template_files($sheet, $opts['template_files']);  
    
  if($arg instanceof node_dto_decorator){
    $decorator = $arg;
  }
  else{
    $decorator = new $opts['decorator_class']($arg);
  }
  
  if(isset($opts['php_functions'])) {
    $decorator->add_php_functions($opts['php_functions']);
  }
  
  $decorator->importStyleSheet($sheet);
  
  return $decorator->transformToXML();
}

/**
 * An example of how to use theme('xsl_theme') with a node object.
 */
function xsl_absolute_render_node($node, $options = array()){
  $style_file = xsl_absolute_gtd() .'node-xsl-absolute.xsl';
  $opts = array(
    'template_files' => array($style_file),
    'template_param_name' => 'node',
    'template_name' => 'node',
  );
  
  $options = array_merge($opts, $options);
  
  return theme('xsl_theme', $node, $options);  
}

/**
 * An example of how to use theme('xsl_theme') with a taxonomy object.
 */
function xsl_absolute_render_taxonomy($node, $options){
  $style_file = xsl_absolute_gtd() .'taxonomy-xsl-absolute.xsl';
  $opts = array(
    'template_files' => array($style_file),
    'template_param_name' => 'node',
    'template_name' => 'taxonomy',
  );
  
  return theme('xsl_theme', $node, $opts);    
}

/**
 * An example of how to use theme('xsl_theme') with a CCK Field.
 */
function xsl_absolute_render_images($node, $preset, $options){
  $sheet = xsl_absolute_build_sheet('images-xsl-absolute.xsl');
  $decorator = new node_dto_decorator($node);
  $decorator->setParameter('preset', $preset);
  
  $opts = array(
    'template_files' => array($sheet),
    'template_param_name' => 'images',
  );  
  
  return theme('xsl_theme', $decorator, $opts);  
}

/**
 * Helper function to quickly build DOMDocuments given a file name
 */
function xsl_absolute_build_sheet($file_name){
  $style_file = xsl_absolute_gtd() .$file_name;
  $doc = new DOMDocument();
  $doc -> load($style_file);
  return $doc;
}

/**
 * Gathers all tmeplate elements within a DOMDocument and imports them to a 'primary' DOMDocument/Stylesheet
 */
function xsl_absolute_import_templates(&$sheet, $dom){
  $templates = $dom->getElementsByTagName('template');
  
  foreach($templates as $template){  
    $local_template = $sheet->importNode($template, true);    
    $sheet->documentElement->appendChild($local_template);
  }
}

/**
 * Imports all template files, into primary stylesheet
 * @param $sheet
 *   DOMDocument object, the primary stylesheet to be invoked
 * @param $template_files
 *   An array of mixed, either strings of file paths or DOMDocuments of loaded stylesheets.
 */
function xsl_absolute_import_template_files(&$sheet, $template_files){
  foreach($template_files as $template_file){    
    if(is_string($template_file) && file_exists($template_file)){
      $doc = new DOMDocument();
      if($doc -> load($template_file)){
        xsl_absolute_import_templates($sheet, $doc);
      }
    }
    else if($template_file instanceof DOMDocument){
      xsl_absolute_import_templates($sheet, $template_file);      
    }  
  }    
}

/**
 * Returns the template directory for executable templates
 */
function xsl_absolute_gtd(){//get template directory
  return drupal_get_path('theme', 'xsl_absolute') .'/templates/xsl-absolute/';  
}


/**
 * Override or insert variables into templates before other preprocess functions have run.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
function xsl_absolute_preprocess(&$vars, $hook) {
  // In D6, the page.tpl uses a different variable name to hold the classes.
  $key = ($hook == 'page' || $hook == 'maintenance_page') ? 'body_classes' : 'classes';

  // Create a D7-standard classes_array variable.
  if (array_key_exists($key, $vars)) {
    // Views (and possibly other modules) have templates with a $classes
    // variable that isn't a string, so we leave those variables alone.
    if (is_string($vars[$key])) {
      $vars['classes_array'] = explode(' ', $vars[$key]);
      unset($vars[$key]);
    }
  }
  else {
    $vars['classes_array'] = array($hook);
  }
  // Add support for Skinr
  if (!empty($vars['skinr']) && array_key_exists('classes_array', $vars)) {
    $vars['classes_array'][] = $vars['skinr'];
  }
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function xsl_absolute_preprocess_page(&$vars, $hook) {
  
  // Add conditional stylesheets.
  if (!module_exists('conditional_styles')) {
    $vars['styles'] .= $vars['conditional_styles'] = variable_get('conditional_styles_' . $GLOBALS['theme'], '');
  }
  if(!isset($vars['classes_array'])){
    $vars['classes_array'] = array();
  }
  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  // Remove the mostly useless page-ARG0 class.
  
  if ($index = array_search(preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. drupal_strtolower(arg(0))), $vars['classes_array'])) {
    unset($vars['classes_array'][$index]);
  }
  if (!$vars['is_front']) {
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    $vars['classes_array'][] = drupal_html_class('page-' . $path);
    // Add unique class for each website section.
    list($section, ) = explode('/', $path, 2);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        $section = 'node-add';
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        $section = 'node-' . arg(2);
      }
    }
    $vars['classes_array'][] = drupal_html_class('section-' . $section);
  }
  if (theme_get_setting('zen_wireframes')) {
    $vars['classes_array'][] = 'with-wireframes'; // Optionally add the wireframes style.
  }
  // We need to re-do the $layout and body classes because
  // template_preprocess_page() assumes sidebars are named 'left' and 'right'.
  $vars['layout'] = 'none';
  if (!empty($vars['sidebar_first'])) {
    $vars['layout'] = 'first';
  }
  if (!empty($vars['sidebar_second'])) {
    $vars['layout'] = ($vars['layout'] == 'first') ? 'both' : 'second';
  }
  // If the layout is 'none', then template_preprocess_page() will already have
  // set a 'no-sidebars' class since it won't find a 'left' or 'right' sidebar.
  if ($vars['layout'] != 'none') {
    // Remove the incorrect 'no-sidebars' class.
    if ($index = array_search('no-sidebars', $vars['classes_array'])) {
      unset($vars['classes_array'][$index]);
    }
    // Set the proper layout body classes.
    if ($vars['layout'] == 'both') {
      $vars['classes_array'][] = 'two-sidebars';
    }
    else {
      $vars['classes_array'][] = 'one-sidebar';
      $vars['classes_array'][] = 'sidebar-' . $vars['layout'];
    }
  }
  // Store the menu item since it has some useful information.
  $vars['menu_item'] = menu_get_item();
  switch ($vars['menu_item']['page_callback']) {
    case 'views_page':
      // Is this a Views page?
      $vars['classes_array'][] = 'page-views';
      break;
    case 'page_manager_page_execute':
      // Is this a Panels page?
      $vars['classes_array'][] = 'page-panels';
      break;
  }
  
  $vars['primary_links_menu'] = theme(array('links__system_main_menu', 'links'), $vars['primary_links'],
                  array(
                    'id' => 'main-menu-links',
                    'class' => 'links clearfix',
                  ),
                  array(
                    'text' => t('Main menu'),
                    'level' => 'h2',
                    'class' => 'element-invisible',
                  ));
   $vars['secondary_links_menu'] = theme(array('links__system_secondary_menu', 'links'), $vars['secondary_links'],
                      array(
                      'id' => 'secondary-menu-links',
                      'class' => 'links clearfix',
                      ),
                      array(
                      'text' => t('Secondary menu'),
                      'level' => 'h2',
                      'class' => 'element-invisible',
                      ));
  
  
}
/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function xsl_absolute_preprocess_node(&$vars, $hook) {
  // Create the build_mode variable.
  $node = $vars['node'];
  $node_dto = new node_dto_decorator($vars);
  $vars['node_dto'] = $node_dto;
  
  switch ($vars['node']->build_mode) {
    case NODE_BUILD_NORMAL:
      $vars['build_mode'] = $vars['teaser'] ? 'teaser' : 'full';
      break;
    case NODE_BUILD_PREVIEW:
      $vars['build_mode'] = 'preview';
      break;
    case NODE_BUILD_SEARCH_INDEX:
      $vars['build_mode'] = 'search_index';
      break;
    case NODE_BUILD_SEARCH_RESULT:
      $vars['build_mode'] = 'search_result';
      break;
    case NODE_BUILD_RSS:
      $vars['build_mode'] = 'rss';
      break;
    case NODE_BUILD_PRINT:
      $vars['build_mode'] = 'print';
      break;
  }

  // Create the user_picture variable.
  $vars['user_picture'] = $vars['picture'];

  // Create the Drupal 7 $display_submitted variable.
  $vars['display_submitted'] = theme_get_setting('toggle_node_info_' . $vars['node']->type);

  // Special classes for nodes.
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $vars['classes_array'][] = drupal_html_class('node-type-' . $vars['type']);
  if ($vars['promote']) {
    $vars['classes_array'][] = 'node-promoted';
  }
  if ($vars['sticky']) {
    $vars['classes_array'][] = 'node-sticky';
  }
  if (!$vars['status']) {
    $vars['classes_array'][] = 'node-unpublished';
    $vars['unpublished'] = 1;
  }
  else {
    $vars['unpublished'] = 0;
  }
  if ($vars['uid'] && $vars['uid'] == $GLOBALS['user']->uid) {
    $vars['classes_array'][] = 'node-by-viewer'; // Node is authored by current user.
  }
  if ($vars['teaser']) {
    $vars['classes_array'][] = 'node-teaser'; // Node is displayed as teaser.
  }
  if (isset($vars['preview'])) {
    $vars['classes_array'][] = 'node-preview';
  }
}

/**
 * Implementation of _preprocess_comment.
 * Adds extra classes for comment new, first/last, anonymous etc
 */
function xsl_absolute_preprocess_comment(&$vars, $hook) {
  // In Drupal 7, $date has been renamed to $created.
  $vars['created'] = $vars['date'];

  // If comment subjects are disabled, don't display them.
  if (variable_get('comment_subject_field_' . $vars['node']->type, 1) == 0) {
    $vars['title'] = '';
  }

  // New comment.
  if ($vars['comment']->new) {
    $vars['classes_array'][] = 'comment-new';
  }
  // Add an "unpublished" flag.
  if ($vars['comment']->status == COMMENT_NOT_PUBLISHED) {
    $vars['unpublished'] = 1;
  }
  else {
    $vars['unpublished'] = 0;
    $vars['classes_array'][] = $vars['status'];
  }
  // Zebra striping.
  if ($vars['id'] == 1) {
    $vars['classes_array'][] = 'first';
  }
  if ($vars['id'] == $vars['node']->comment_count) {
    $vars['classes_array'][] = 'last';
  }
  $vars['classes_array'][] = $vars['zebra'];
  if ($vars['comment']->uid == 0) {
    // Comment is by an anonymous user.
    $vars['classes_array'][] = 'comment-by-anonymous';
  }
  else {
    if ($vars['comment']->uid == $vars['node']->uid) {
      // Comment is by the node author.
      $vars['classes_array'][] = 'comment-by-node-author';
    }
    if ($vars['comment']->uid == $GLOBALS['user']->uid) {
      // Comment was posted by current user.
      $vars['classes_array'][] = 'comment-by-viewer';
    }
  }
}

/**
 * Preprocess variables for region.tpl.php
 *
 * Prepare the values passed to the theme_region function to be passed into a
 * pluggable template engine. Uses the region name to generate a template file
 * suggestions. If none are found, the default region.tpl.php is used.
 *
 * @see region.tpl.php
 */
function xsl_absolute_preprocess_region(&$vars, $hook) {
  // Create the $content variable that templates expect.
  
  $vars['content'] = $vars['elements']['#children'];
  $vars['region'] = $vars['elements']['#region'];

  // Setup the default classes.
  $region = 'region-' . str_replace('_', '-', $vars['region']);
  $vars['classes_array'] = array('region', $region);

  // Sidebar regions get a common template suggestion a couple extra classes.
  if (strpos($vars['region'], 'sidebar_') === 0) {
    $vars['template_files'][] = 'region-sidebar';
    $vars['classes_array'][] = 'column';
    $vars['classes_array'][] = 'sidebar';
  }

  // Setup the default template suggestion.
  $vars['template_files'][] = $region;
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function xsl_absolute_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  // Drupal 7 uses a $content variable instead of $block->content.
  $vars['content'] = $block->content;
  // Drupal 7 should use a $title variable instead of $block->subject.
  $vars['title'] = $block->subject;

  // Special classes for blocks.
  $vars['classes_array'][] = 'block-' . $block->module;
  $vars['classes_array'][] = 'region-' . $vars['block_zebra'];
  $vars['classes_array'][] = $vars['zebra'];
  $vars['classes_array'][] = 'region-count-' . $vars['block_id'];
  $vars['classes_array'][] = 'count-' . $vars['id'];

  // Create the block ID.
  $vars['block_html_id'] = 'block-' . $block->module . '-' . $block->delta;

  $vars['edit_links_array'] = array();
  
}

/**
 * Override or insert variables into the views-view templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("views-view" in this case.)
 */
function xsl_absolute_preprocess_views_view(&$vars, $hook) {
  // Add the default Views classes.
  $vars['classes_array'][0] = 'view'; // Replace "views-view".
  $vars['classes_array'][] = 'view-' . $vars['css_name'];
  $vars['classes_array'][] = 'view-id-' . $vars['name'];
  $vars['classes_array'][] = 'view-display-id-' . $vars['display_id'];
  $vars['classes_array'][] = 'view-dom-id-' . $vars['dom_id'];
}

/**
 * Implements theme_menu_item_link()
 */
function xsl_absolute_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>';
    $link['localized_options']['html'] = TRUE;
  }

  return l($link['title'], $link['href'], $link['localized_options']);
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function xsl_absolute_menu_local_tasks() {
  $output = '';

  // CTools requires a different set of local task functions.
  if (module_exists('ctools')) {
    ctools_include('menu');
    $primary = ctools_menu_primary_local_tasks();
    $secondary = ctools_menu_secondary_local_tasks();
  }
  else {
    $primary = menu_primary_local_tasks();
    $secondary = menu_secondary_local_tasks();
  }

  if ($primary) {
    $output .= '<ul class="tabs primary clearfix">' . $primary . '</ul>';
  }
  if ($secondary) {
    $output .= '<ul class="tabs secondary clearfix">' . $secondary . '</ul>';
  }

  return $output;
}

if (!function_exists('drupal_html_class')) {
  /**
   * Prepare a string for use as a valid class name.
   *
   * Do not pass one string containing multiple classes as they will be
   * incorrectly concatenated with dashes, i.e. "one two" will become "one-two".
   *
   * @param $class
   *   The class name to clean.
   * @return
   *   The cleaned class name.
   */
  function drupal_html_class($class) {
    // By default, we filter using Drupal's coding standards.
    $class = strtr(drupal_strtolower($class), array(' ' => '-', '_' => '-', '/' => '-', '[' => '-', ']' => ''));

    // http://www.w3.org/TR/CSS21/syndata.html#characters shows the syntax for valid
    // CSS identifiers (including element names, classes, and IDs in selectors.)
    //
    // Valid characters in a CSS identifier are:
    // - the hyphen (U+002D)
    // - a-z (U+0030 - U+0039)
    // - A-Z (U+0041 - U+005A)
    // - the underscore (U+005F)
    // - 0-9 (U+0061 - U+007A)
    // - ISO 10646 characters U+00A1 and higher
    // We strip out any character not in the above list.
    $class = preg_replace('/[^\x{002D}\x{0030}-\x{0039}\x{0041}-\x{005A}\x{005F}\x{0061}-\x{007A}\x{00A1}-\x{FFFF}]/u', '', $class);

    return $class;
  }
} /* End of drupal_html_class conditional definition. */

if (!function_exists('drupal_html_id')) {
  /**
   * Prepare a string for use as a valid HTML ID and guarantee uniqueness.
   *
   * @param $id
   *   The ID to clean.
   * @return
   *   The cleaned ID.
   */
  function drupal_html_id($id) {
    $id = strtr(drupal_strtolower($id), array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));

    // As defined in http://www.w3.org/TR/html4/types.html#type-name, HTML IDs can
    // only contain letters, digits ([0-9]), hyphens ("-"), underscores ("_"),
    // colons (":"), and periods ("."). We strip out any character not in that
    // list. Note that the CSS spec doesn't allow colons or periods in identifiers
    // (http://www.w3.org/TR/CSS21/syndata.html#characters), so we strip those two
    // characters as well.
    $id = preg_replace('/[^A-Za-z0-9\-_]/', '', $id);

    return $id;
  }
} /* End of drupal_html_id conditional definition. */
